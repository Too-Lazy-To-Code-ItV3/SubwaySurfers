using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameScene : MonoBehaviour
{
    public Light directionalLight; // Assign the light in the Inspector

    private void Start()
    {
        // Check if a light is assigned
        if (directionalLight != null)
        {
            // Set the light's color to white (assuming it's a directional light)
            directionalLight.color = Color.white;
        }
    }
}
