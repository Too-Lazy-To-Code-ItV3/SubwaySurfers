using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSpawn : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> PrefabsList = new List<GameObject>();
    private List<GameObject> BuildList = new List<GameObject>();
    public static Vector3 ResetPos;
    // Start is called before the first frame update
    void Start()
    {
        ResetPos = transform.position;
        foreach (GameObject _go in PrefabsList)
        {
            GameObject _newObs = Instantiate(_go);
            BuildList.Add(_newObs);

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
