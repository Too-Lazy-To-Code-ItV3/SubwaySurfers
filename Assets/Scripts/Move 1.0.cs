using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class Testing : MonoBehaviour
{
    private float xMin = -8.0f, xMax = 8.0f;
    public float moveSpeed = 10.0f;
    private float sideStepDistance = 8.0f;
    public float jumpForce = 10.0f;
    private bool isMoving;
    private bool isJumping;

    Vector3 targetPositionX;
    private Rigidbody rb;
    private bool onGround;
    private bool isDown;
    private bool isRoll;
    Quaternion y;


    public static int score;
   


    void Start()
    {
        score = 0;
        targetPositionX = transform.position;
        isMoving = false;
        isJumping = false;
        onGround = false;
        isDown = false;
        isRoll = false;
        rb = GetComponent<Rigidbody>();

    }

    void Update()
    {
        if (Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") < 0 && !isMoving && Mathf.Clamp(transform.position.x, xMin, xMax) > -8.0)
        {
            Left();  
        }
        else if (Input.GetButtonDown("Horizontal") && Input.GetAxisRaw("Horizontal") > 0 && !isMoving && Mathf.Clamp(transform.position.x, xMin, xMax) < 8.0f)
        {
            Right();
        }
        else if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical")>0 && onGround && !isMoving)
        {
            Jump();
        }
        else if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0 && !isMoving && !isDown && !onGround)
        {
            isDown = true;
        }
        else if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0 && !isMoving && !isDown && onGround)
        {
            isRoll = true;
        }

        if (isMoving)
        {
            float step = moveSpeed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, targetPositionX, step);

            if (transform.position == targetPositionX)
            {
                isMoving = false;
            }
        }

        Debug.Log("SCORE : " + score);
    }

    [System.Obsolete]
    void FixedUpdate()
    {
        if (isJumping)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isJumping = false;
        }
        if(isDown)
        {
            rb.AddForce(Vector3.down * jumpForce, ForceMode.Impulse);
            isJumping = false;
            isDown = false;

        }
        if(isRoll)
        {
           
            StartCoroutine(RotateAndBack()); 

        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ground"))
        {
            onGround = true;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ground"))
        {
            onGround = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ground"))
        {
            onGround = false;
        }
    }

    void Left()
    {
        targetPositionX = transform.position - (Vector3.right * sideStepDistance);
        isMoving = true;
    }
    void Right()
    {
        targetPositionX = transform.position + (Vector3.right * sideStepDistance);
        isMoving = true;
    }
    void Jump()
    {
        if (onGround)
        {
            isJumping = true;
        }
    }


    [System.Obsolete]
    IEnumerator RotateAndBack() //Call this method with StartCoroutine(RotateForSeconds());
    {
        while(isRoll) {
            //transform.Rotate(500 * Vector3.left * Time.deltaTime);
            transform.rotation = Quaternion.EulerAngles(-60.0f, 0.0f, 0.0f);
            yield return new WaitForSeconds(1);
            transform.rotation = Quaternion.EulerRotation(0.0f, 0.0f, 0.0f);
            isRoll = false;
        }
         

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Coins"))
            score += 5;
    }
}

