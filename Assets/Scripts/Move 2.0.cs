using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MoveAn : MonoBehaviour
{
    public TMP_Text scoreText;
    Vector3 targetPositionX;
    Rigidbody rb;
    Animator An;
    private float xMin = -8.0f, xMax = 8.0f;
    public float moveSpeed = 20.0f;
    private float sideStepDistance = 8.0f;
    [SerializeField]
    public float jumpHeight = 7.0f;

    float jumpForce;
    private bool isMoving;
    int ground;
    int down;
    int jump;
    int fall;
    int roll;



    public static int game;
    public static int score;

    private bool isJumping;
    private bool onGround;
    private bool isDown;
    bool isFalling;
    private bool isRoll;


   
    void Start()
    {
        game = 1
;        transform.position = new Vector3(0.0f, transform.position.y, transform.position.z);
        An = GetComponent<Animator>();
        score = 0;
        targetPositionX = transform.position;
        rb = GetComponent<Rigidbody>();
        ground = Animator.StringToHash("onGround");
        jump = Animator.StringToHash("isJumping");
        fall = Animator.StringToHash("isFalling");
        roll = Animator.StringToHash("isRoll");
        jumpForce = Mathf.Sqrt(2*jumpHeight * Mathf.Abs(Physics.gravity.y) * rb.mass);
        
    }

    void Update()
    {
        if (game == 0)
            SceneManager.LoadScene("Demo1");

        isJumping = An.GetBool(jump);
        onGround = An.GetBool(ground);
        isFalling = An.GetBool(fall);
        isRoll = An.GetBool(roll);
        if (Input.GetButtonDown("Horizontal") && Input.GetAxis("Horizontal") < 0 && !isMoving && transform.position.x > xMin + 0.1f && rb.velocity.y >= 0)
        {
            Left();
        }
        if (Input.GetButtonDown("Horizontal") && Input.GetAxis("Horizontal") > 0 && !isMoving && transform.position.x < xMax && rb.velocity.y >= 0)
        {
            Right();
        }
        if (Input.GetButtonDown("Vertical") && Input.GetAxis("Vertical") > 0 && onGround && !isMoving)
        {
            Jump();
        }
        if (Input.GetButtonDown("Vertical") && Input.GetAxis("Vertical") < 0 && !isMoving && !isDown && !onGround)
        {
            An.SetBool(fall, true);
            isDown = true;
        }
        if (Input.GetButtonDown("Vertical") && Input.GetAxisRaw("Vertical") < 0 && !isMoving && !isDown && onGround)
        {
            An.SetBool(roll, true);

        }
        if (Input.GetButtonDown("Cancel") && onGround)
            SceneManager.LoadScene("Demo1");

        if (isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPositionX, moveSpeed * Time.deltaTime);

            if (transform.position == targetPositionX)
            {
                isMoving = false;

            }
        }


    }


    void FixedUpdate()
    {
        if (isJumping)
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            An.SetBool(jump, false);
        }
        if (rb.velocity.y < 0 && !onGround)
        {

            An.SetBool(fall, true);
        }
        else
        {
            An.SetBool(fall, false);

        }
        if (isDown)
        {
            rb.AddForce(Vector3.down * jumpForce, ForceMode.Impulse);
            isDown = false;

        }
        if (isRoll)
        {

            An.SetBool(roll, false);
        }

    }
    /********************************MOVEMENT FUNCTIONS***************************************/
    void Left()
    {
        targetPositionX = transform.position - (Vector3.right * sideStepDistance);
        isMoving = true;

    }
    void Right()
    {
        targetPositionX = transform.position + (Vector3.right * sideStepDistance);
        isMoving = true;
    }
    void Jump()
    {
        if (onGround)
        {
            An.SetBool(jump, true);
        }
    }

    /****************************************************COLLISION***********************************************/
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ground"))
        {
            An.SetBool(fall, false);
            An.SetBool(ground, true);
        }
        Debug.Log(collision.gameObject.name);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ground"))
        {
           
            An.SetBool(ground, true);
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.Equals("Ground"))
        {
            An.SetBool(ground, false);

        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Coins"))
        {
            score += 5;
            scoreText.text = "Score : " + score;
            other.gameObject.transform.position = TrainSpawn.ResetPos;
            

        }
        if (other.gameObject.tag.Equals("Obs"))
            game = 0;
            

    }
}

