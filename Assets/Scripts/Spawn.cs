using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TrainSpawn : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    public GameObject StarFab;
    [SerializeField]
    private List<GameObject> PrefabsList = new List<GameObject>();
    private List<GameObject> ObsList = new List<GameObject>();
    private List<GameObject> StarsList = new List<GameObject>();
    public static List<GameObject> ActiveList = new List<GameObject>();
    public static Vector3 ResetPos;

    [SerializeField]
    public static float Speed = 10.0f;


    public float spawnAreaWidth = 20.0f;
    public float initialSpawnDelay = 2.0f;
    [SerializeField]
    public float spawnInterval =6f;

    void Start()
    {

        Vector3 idk = transform.position;
        ResetPos = transform.position;
        for (int i = 0; i < spawnAreaWidth; i++)
        {
            foreach (GameObject _go in PrefabsList)
            {
                GameObject _newObs = Instantiate(_go, transform.position, Quaternion.identity);
                ObsList.Add(_newObs);

            }
        }

        for (int i = 0; i < 5; i++)
        {
            GameObject _newStar = Instantiate(StarFab, idk, Quaternion.identity);
            StarsList.Add(_newStar);
            idk += Vector3.forward * 10;
        }
        SpawnRandomObjects();
        StartCoroutine(SpawnObjectsPeriodically());
    }

    IEnumerator SpawnObjectsPeriodically()
    {
        while (MoveAn.game!=0)
        {
            yield return new WaitForSeconds(spawnInterval);
            SpawnRandomObjects();

        }
    }


    public void SpawnRandomObjects()
    {
        int randomIndex1 = Random.Range(0, ObsList.Count);
        int randomIndex2;
        do { randomIndex2 = Random.Range(0, ObsList.Count); } while (randomIndex2 == randomIndex1);

        GameObject prefab1 = ObsList[randomIndex1];
        GameObject prefab2 = ObsList[randomIndex2];

        float randomZ = Random.Range(-spawnAreaWidth / 2, spawnAreaWidth / 2);
        
        float randomX1;
        float randomX2;
        float randomX3;

        float randomSpawnArea = Random.Range(0, 3);

        if (randomSpawnArea == 0)
        {
            randomX1 = 8;
            randomX2 = -8;
            randomX3 = 0;
        }
        else if (randomSpawnArea == 1)
        {
            randomX1 = 0;
            randomX2 = 8;
            randomX3 = -8;
        }
        else
        {
            randomX1 = -8;
            randomX2 = 0;
            randomX3 = 8;
        }

        prefab1.transform.position = new Vector3(randomX1, -1.0f, randomZ);
        prefab2.transform.position = new Vector3(randomX2, -1.0f, randomZ);
        foreach (GameObject _star in StarsList)
        {
            float offsetZ = Random.Range(-spawnAreaWidth / 2, spawnAreaWidth / 2);
            _star.transform.position = new Vector3(randomX3, 1.0f, randomZ+offsetZ+_star.transform.position.z);

        }
    }


}
