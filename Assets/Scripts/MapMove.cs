using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMove : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      transform.Translate(Vector3.back * TrainSpawn.Speed * Time.deltaTime,Space.World);

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Reset"))
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, MapSpawn.ResetPos.z);
        }
    }

}
