using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class movingle : MonoBehaviour
{
    

    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
        if (MoveAn.game == 0 || transform.position.y == TrainSpawn.ResetPos.y )
            return;

        transform.Translate(Vector3.back * TrainSpawn.Speed * Time.deltaTime);


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name.Equals("Reset"))
        {
            transform.position = TrainSpawn.ResetPos;
        }
    }
}
